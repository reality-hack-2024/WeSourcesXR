using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowComments : MonoBehaviour
{
    private GameObject comment;

    void Start()
    {
        comment = transform.GetChild(0).GetChild(0).gameObject;
    }

    public void ToggleCommentsPanel()
    {
        if (!comment.activeSelf)
            comment.SetActive(true);
        else
            comment.SetActive(false);
    }
}
