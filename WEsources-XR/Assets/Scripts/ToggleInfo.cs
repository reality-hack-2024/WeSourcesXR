using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInfo : MonoBehaviour
{
    private GameObject info;

    void Start()
    {
        info = transform.GetChild(0).gameObject;
    }

    public void ToggleInfoPanel()
    {
        if (!info.activeSelf)
            info.SetActive(true);
        else
            info.SetActive(false);
    }
}
