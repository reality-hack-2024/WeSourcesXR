# WeSources

## Setup

**Clone the project from <https://codeberg.org/reality-hack-2024/TABLE_92>**
-   The MR project files will be in the `WEsources-XR` folder
-   The AR project files will be in a zipped file called `wesources lens studio.zip`

**Unity Project**

* Download Unity and version 20222.3.6 LTS of the engine
* Connect Meta Quest 3 headset and Leap Motion Controller 2
* Install the Ultraleap Meta Installer APKs
* Ensure hand tracking is enabled on Meta Quest 3

**Lens Studio**

* Download Lens Studio
* Unzip the WeSources Lens Studio File

### Hardware Required

- Meta Quest 3
- Ultraleap Leap Motion Controller 2
- Smartphone with Snapchat

### Software Dependencies

- Unity `2022.3.6 LTS`
- Ultraleap Meta Installer APKs
- Lens Studio `v4.55.1 or higher`
- Snapchat for iOS or Android

## Run

**Unity Project**

- Sideload the APK file to the Meta Quest 3 and launch WEsources XR app
- For example: `adb install "WEsources XR.apk"` (make sure adb binary is installed on your machine)

**Lens Studio**

- Open the Lens Studio, Click Open Project, Select the unzipped WeSources Lens Studio File
- The project should be ready to preview, run, and publish to Snapchat for AR use
- Click either `Publish Lens` or `Send to Snapchat` to activate. Alternatively view app in the `Preview` window

or

-Try it out [here](https://www.snapchat.com/unlock/?type=SNAPCODE&uuid=8bbd0f95c85045de978a6dd29a83c513&metadata=01)


## Shout-Outs

We were supported by the many mentors, sponsors, and volunteers of the MIT Reality Hack and are eternally grateful for helping us to bring our project into existence!
